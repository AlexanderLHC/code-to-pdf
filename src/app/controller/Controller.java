package app.controller;

import java.net.URL;
import java.util.ResourceBundle;

import app.model.Directory;
import app.model.Document;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class Controller implements Initializable {
	@FXML
	private TextField txfTitle;
	@FXML
	private TextField txfAuthor;
	@FXML
	private TextField txfDirSource;
	@FXML
	private TextField txfDirDestination;
	@FXML
	private ListView<String> lwDirectories;
	@FXML
	private VBox vbTargetFiles;
	@FXML
	private TextArea txaOutput;

	private ObservableList<String> files = FXCollections.observableArrayList();
	private Directory root;
	private Directory destination;

	private Stage primaryStage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lwDirectories.setItems(files);
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	@FXML
	private void chooseSource() {
		DirectoryChooser dirChooser = new DirectoryChooser();

		try {
			root = new Directory(dirChooser.showDialog(primaryStage).getAbsolutePath());
			txfDirSource.setText(root.getRoot());
			files.setAll(root.getFiles());
			vbTargetFiles.setVisible(true);
		} catch (Exception e) {
			vbTargetFiles.setVisible(false);
			txfDirSource.setText("invalid directory");
			e.printStackTrace();
		}
	}

	@FXML
	private void chooseDestination() {
		DirectoryChooser dirChooser = new DirectoryChooser();

		try {
			destination = new Directory(dirChooser.showDialog(primaryStage).getAbsolutePath());
			txfDirDestination.setText(destination.getRoot());
		} catch (Exception e) {
			txfDirDestination.setText("invalid directory");
			e.printStackTrace();
		}
	}

	@FXML
	private void runCompiler() {
		if (canCompile()) {
			Document doc = new Document();
			doc.setTitle(txfTitle.getText());
			doc.setAuthor(txfAuthor.getText());
			doc.setCodeDir(root);
			doc.setDestination(destination);
			doc.writeTexFiles();
			txaOutput.setText(doc.compile());
			txaOutput.appendText("Finished compiling!");
		}
	}

	private boolean canCompile() {
		boolean compileable = false;
		if (root != null
				&& !txfTitle.getText().equals("")
				&& !txfAuthor.getText().equals("")
				&& !txfDirDestination.getText().equals("")) {
			compileable = true;
		}
		return compileable;
	}
}
