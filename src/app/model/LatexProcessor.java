package app.model;

import java.util.List;

public class LatexProcessor {

	public static String texCode(List<String> files, String root) {
		StringBuilder sb = new StringBuilder();

		for (String file : files) {
			String title = file.split(root)[1];
			sb.append(String.format("\\chapter{%s}%n", title));
			sb.append(String.format("\\lstinputlisting[language=java]{%s}%n", file));
			sb.append("\\newpage\n");
		}
		return sb.toString();
	}

	public static String texProperties(String title, String author) {
		StringBuilder sb = new StringBuilder();

		String texTitle = String.format("\\title{%s}%n", title);
		String texAuthor = String.format("\\author{%s}%n", author);
		sb.append(texTitle);
		sb.append(texAuthor);

		return sb.toString();
	}
}
