package app.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Terminal {
	private File texDir;

	public Terminal() {
		texDir = new File("tex/");
	}

	public String compileTexToPdf() {
		StringBuilder output = new StringBuilder();
		try {
			String[] compile = { "latexmk", "-pdf", "rapport.tex" };
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.directory(texDir);
			processBuilder.command(compile);
			Process process = processBuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line);
			}
			int exitCode = process.waitFor();
			output.append("\nExited with error code : " + exitCode + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return output.toString();
	}

	public String cleanLatexFiles() {
		StringBuilder output = new StringBuilder();
		try {
			String[] clean = { "latexmk", "-c", "rapport.tex" };
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.directory(texDir);
			processBuilder.command(clean);
			Process process = processBuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line);
			}
			int exitCode = process.waitFor();
			output.append("\nExited with code : " + exitCode + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return output.toString();
	}

	public String movePdfToDestination(Path destinationDir, String filename) {
		Path source = Paths.get(texDir.getAbsolutePath() + "/rapport.pdf");
		Path destination = Paths.get(destinationDir + "/" + filename);
		String output = "";
		try {
			Files.move(source, destination);
			output = "Finished moving your report" + "\n";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return output;
	}

}
