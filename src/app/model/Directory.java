package app.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Directory {

	private String root;
	private List<String> files;

	public Directory(String root) {
		this.root = root;
		setFiles();
	}

	public void setFiles() {
		try (Stream<Path> walk = Files.walk(Paths.get(root))) {
			files = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".java")).collect(Collectors.toList());
		} catch (IOException e) {
			files = null;
			e.printStackTrace();
		}
	}

	public ObservableList<String> getFiles() {
		ObservableList<String> obsFiles = FXCollections.observableArrayList();
		obsFiles.setAll(files);
		return obsFiles;
	}

	public String getRoot() {
		return root;
	}

}
