package app.model;

import java.io.PrintWriter;
import java.nio.file.Paths;

public class Document {
	private String title;
	private String author;
	private Directory codeDir;
	private Directory destination;

	private final String TMP_DIR = "./tex/temporary/";

	public Document() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Directory getCodeDir() {
		return codeDir;
	}

	public void setCodeDir(Directory codeDir) {
		this.codeDir = codeDir;
	}

	public Directory getDestination() {
		return destination;
	}

	public void setDestination(Directory destination) {
		this.destination = destination;
	}

	private void writeProperties() {
		String properties = LatexProcessor.texProperties(title, author);
		try (PrintWriter writer = new PrintWriter(TMP_DIR + "properties.tex")) {
			writer.write(properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeCode() {
		String code = LatexProcessor.texCode(codeDir.getFiles(), codeDir.getRoot());
		try (PrintWriter writer = new PrintWriter(TMP_DIR + "code.tex")) {
			writer.write(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void writeTexFiles() {
		writeProperties();
		writeCode();
	}

	public String compile() {
		Terminal terminal = new Terminal();
		StringBuilder sb = new StringBuilder();
		sb.append(terminal.compileTexToPdf());
		sb.append(terminal.cleanLatexFiles());
		String fileTitle = title.trim().replaceAll(" ", "_") + ".pdf";
		sb.append(terminal.movePdfToDestination(Paths.get(destination.getRoot()), fileTitle));
		return sb.toString();
	}
}
